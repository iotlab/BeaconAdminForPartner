/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('AdvertisementListCtrl', ['$cookieStore', '$scope', '$modal', '$route',
    'BeaconService', 'AdvertisementService', 'ErrorCMDService', 'SessionService',
    function ($cookieStore, $scope, $modal, $route,
            BeaconService, AdvertisementService, ErrorCMDService, SessionService) {

        $scope.getAdmin = function ()
        {
            return $cookieStore.get('user').isAdmin;
        };

        //$scope.isAdmin = $scope.getAdmin();
        $scope.isAdmin = true;
        $scope.showPartnerList = false;
        $scope.beaconId = -1;

        $scope.createAdvertisement = function () {
            console.debug('create Advertisement');
            $modal.open({
                templateUrl: 'views/createnewadvertisement.html',
                controller: 'CreateNewAdvertisementCtrl',
                resolve: {
                    beacon: function () {
                        return undefined;
                    },
                    cb: function () {
                        return $scope.updateDisplay;
                    }
                }
            });
        };
        
         $scope.editAdvertisement = function (advertisement) {
            console.debug('editAdvertisement');

            $modal.open({
                templateUrl: 'views/editadvertisement.html',
                controller: 'EditAdvertisementCtrl',
                resolve: {
                    advertisement: function () {
                        return advertisement;
                    },
                    cb: function () {
                        return $scope.updateDisplay;
                    }
                }
            });
        };
        
        
        $scope.viewAdvertisementDetail = function (adId)
        {
            var resp = BeaconService.getBeacon({
                beaconId: beaconId
            }, function ()
            {
                if (resp.error === 0)
                {
                    $modal.open({
                        templateUrl: 'views/beacondetail.html',
                        controller: 'BeaconDetailCtrl',
                        resolve: {
                            beacon: function () {
                                var beacon = resp.data;
                                beacon.beaconId = beaconId;
                                return beacon;
                            },
                            cb: function () {
                                return $scope.updateDisplay;
                            }
                        }
                    });
                }
                else
                {
                    ErrorCMDService.displayError(resp.message);
                }

            });
        };
        $scope.removeAdvertisement = function (adId)
        {

         console.log("remove advertisement");
            var cb = function () {
                var resp = AdvertisementService.removeAdvertisement({
                    advertismentId: adId

                }, function () {

                    if (resp.error === 0)
                    {
                        $scope.updateDisplay();
                    }
                    else
                    {
                        //ErrorCMDService.displayError(resp.message);
                    }
                });
            };
            $modal.open({
                templateUrl: 'views/submitmodal.html',
                controller: 'SubmitModalCtrl',
                size: 'sm',
                resolve: {
                    cb: function () {
                        return cb;
                    }
                }
            });
        };

        $scope.updateBeaconInfo = function () {
            var resp = BeaconService.addBeacon({
                name: $scope.currentBeacon.beaconName,
                description: $scope.currentBeacon.description,
                uuid: $scope.currentBeacon.uuid,
                major: $scope.currentBeacon.bMajor,
                minor: $scope.currentBeacon.bMinor,
                lat: $scope.currentBeacon.lat,
                lng: $scope.currentBeacon.lng,
                enable: $scope.currentBeacon.enable

            }, function () {
                if (resp.error === 0) {
                    //$scope.ok();
                    //cb();
                    alert('Update thành công');
                }
                else {
                    alert('Update thất bại\n\r message: ' + resp.message + ' error: ' + resp.error);
                }

            });
        };
        
        $scope.getAdvertisementUri = function (adId)
        {
            return '#advertises/' + adId;
        };

        $scope.pageChanged = function ()
        {
//            if ($scope.showPartnerList) {
//                $scope.partnerPageChanged();
//                return;
//            }
            var start = ($scope.currentPage - 1) * $scope.itemPerPage;
            var apiResp = AdvertisementService.listAdvertisement({
                session: SessionService.getSessionId(),
                start: start,
                length: $scope.itemPerPage

            }, function () {

                if (apiResp.error === 0) {
                    var advertisements = apiResp.data.advertisements;
//		    for (var i = 0; i < beacons.length; ++i) {
//			var d = new Date(beacons[i].createTime * 1000);
//			beacons[i].dateCreated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//			d = new Date(beacons[i].lastUpdate * 1000);
//			beacons[i].dateUpdated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//		    }
                    $scope.advertisementList = advertisements;
                    $scope.advertisements = [].concat($scope.advertisementList);
                    $scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
                    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
                }
                else
                {
                    //ErrorCMDService.displayError(apiResp.message);
                }
            });
        };

        $scope.updateDisplay = function () {
            $scope.pageChanged();
        };
//	$scope.setPage = function (pageNo) {
//	    $scope.currentPage = pageNo;
//	};
        $scope.init = function () {
            var user = SessionService.getUser();
            if (user === undefined || user === null) {
                console.log('chua dang nhap');
                window.location.replace('#/login');
                return;
            }
            console.log('dang nhap roi');
            
            $scope.$route = $route;
            //Pagination
            $scope.totalItems = 0;
            $scope.itemPerPage = 10;
            $scope.currentPage = 1;
            var sessionId = SessionService.getSessionId();
            if (sessionId)
            {
                $scope.updateDisplay();
            } else {
                $scope.updateDisplay();
            }
        };
        $scope.init();
    }
]);
