/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';
var app = angular.module('myApp');
app.controller('BeaconListCtrl', ['$cookieStore', '$scope', '$modal', '$route',
    'BeaconService', 'ErrorCMDService', 'SessionService',
    function ($cookieStore, $scope, $modal, $route,
            BeaconService, ErrorCMDService, SessionService) {

        $scope.getAdmin = function ()
        {
            return $cookieStore.get('user').isAdmin;
        };

        $scope.showAdsPopup = function (beaconId) {
            console.log('BeaconList get list Advertis with beaconId ' + beaconId);
            $modal.open({
                templateUrl: 'views/beaconAdvertisment.html',
                controller: 'BeaconAdvertisCtrl',
                resolve: {
                    beaconId: function () {
                        return beaconId;
                    },
                    cb: function () {
                        return $scope.updateDisplay;
                    }
                }
            });
        };

        //$scope.isAdmin = $scope.getAdmin();
        $scope.isAdmin = true;
        $scope.showPartnerList = false;
        $scope.beaconId = -1;

        $scope.createBeacon = function () {

            $modal.open({
                templateUrl: 'views/createnewbeacon.html',
                controller: 'CreateNewBeaconCtrl',
                resolve: {
                    beacon: function () {
                        return undefined;
                    },
                    cb: function () {
                        return $scope.updateDisplay;
                    }
                }
            });
        };
        $scope.viewPartnerList = function (beacon)
        {
            $scope.currentBeacon = beacon;
            $scope.beaconId = beacon.beaconId;
            $scope.showPartnerList = true;
            $scope.partnerPageChanged();
        };
        $scope.viewBeaconDetail = function (beaconId)
        {
            var resp = BeaconService.getBeacon({
                beaconId: beaconId
            }, function ()
            {
                if (resp.error === 0)
                {
                    $modal.open({
                        templateUrl: 'views/beacondetail.html',
                        controller: 'BeaconDetailCtrl',
                        resolve: {
                            beacon: function () {
                                var beacon = resp.data;
                                beacon.beaconId = beaconId;
                                return beacon;
                            },
                            cb: function () {
                                return $scope.updateDisplay;
                            }
                        }
                    });
                }
                else
                {
                    //ErrorCMDService.displayError(resp.message);
                }

            });
        };
        $scope.removeBeacon = function (beaconId)
        {

            var cb = function () {
                var resp = BeaconService.removeBeacon({
                    beaconId: beaconId

                }, function () {

                    if (resp.error === 0)
                    {
                        $scope.updateDisplay();
                    }
                    else
                    {
                        ErrorCMDService.displayError(resp.message);
                    }
                });
            };
            $modal.open({
                templateUrl: 'views/submitmodal.html',
                controller: 'SubmitModalCtrl',
                size: 'sm',
                resolve: {
                    cb: function () {
                        return cb;
                    }
                }
            });
        };

        $scope.removePartnerFromBeacon = function (partnerId)
        {

            var cb = function () {
                var resp = BeaconService.removePartner({
                    beaconId: $scope.beaconId,
                    partnerId: partnerId

                }, function () {

                    if (resp.error === 0)
                    {
                        $scope.updateDisplay();
                    }
                    else
                    {
                        ErrorCMDService.displayError(resp.message);
                    }
                });
            };
            $modal.open({
                templateUrl: 'views/submitmodal.html',
                controller: 'SubmitModalCtrl',
                size: 'sm',
                resolve: {
                    cb: function () {
                        return cb;
                    }
                }
            });
        };

        $scope.backToBeaconList = function ()
        {
            $scope.beaconId = -1;
            $scope.showPartnerList = false;
            $scope.pageChanged();
        };

        $scope.updateBeaconInfo = function () {
            var resp = BeaconService.addBeacon({
                name: $scope.currentBeacon.beaconName,
                description: $scope.currentBeacon.description,
                uuid: $scope.currentBeacon.uuid,
                major: $scope.currentBeacon.bMajor,
                minor: $scope.currentBeacon.bMinor,
                lat: $scope.currentBeacon.lat,
                lng: $scope.currentBeacon.lng,
                enable: $scope.currentBeacon.enable

            }, function () {
                if (resp.error === 0) {
                    //$scope.ok();
                    //cb();
                    alert("Update thành công");
                }
                else {
                    alert("Update thất bại\n\r message: " + resp.message + " error: " + resp.error);
                }

            });
        };
        $scope.addPartner = function () {

            $modal.open({
                templateUrl: 'views/addpartnertobeacon2.html',
                controller: 'PartnerListCtrl2',
                resolve: {
                    beaconId: function () {
                        return $scope.beaconId;
                    },
                    cb: function () {
                        return $scope.partnerPageChanged;
                    }
                }
            });
        };

        $scope.getBeaconUri = function (beaconId)
        {
            console.log('getBeaconUri' + beaconId);
            //return '#beacons/' + beaconId;
        };

        $scope.pageChanged = function ()
        {
            if ($scope.showPartnerList) {
                $scope.partnerPageChanged();
                return;
            }
            var start = ($scope.currentPage - 1) * $scope.itemPerPage;
            var apiResp = BeaconService.listBeacon({
                session: SessionService.getSessionId(),
                start: start,
                length: $scope.itemPerPage

            }, function () {

                if (apiResp.error === 0) {
                    var beacons = apiResp.data.beacons;
//		    for (var i = 0; i < beacons.length; ++i) {
//			var d = new Date(beacons[i].createTime * 1000);
//			beacons[i].dateCreated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//			d = new Date(beacons[i].lastUpdate * 1000);
//			beacons[i].dateUpdated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//		    }
                    $scope.beaconList = beacons;
                    $scope.beacons = [].concat($scope.beaconList);
                    $scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
                    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
                }
                else
                {
                    ErrorCMDService.displayError(apiResp.message);
                }
            });
        };
        $scope.partnerPageChanged = function ()
        {
            var start = ($scope.currentPage - 1) * $scope.itemPerPage;
            var apiResp = BeaconService.listPartner({
                beaconId: $scope.beaconId,
                start: start,
                length: $scope.itemPerPage

            }, function () {

                if (apiResp.error === 0) {
                    var partners = apiResp.data.partners;
//		    for (var i = 0; i < beacons.length; ++i) {
//			var d = new Date(beacons[i].createTime * 1000);
//			beacons[i].dateCreated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//			d = new Date(beacons[i].lastUpdate * 1000);
//			beacons[i].dateUpdated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//		    }
                    $scope.partnerList = partners;
                    $scope.partners = [].concat($scope.partnerList);
                    $scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
                    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
                }
                else
                {
                    ErrorCMDService.displayError(apiResp.message);
                }
            });
        };

        $scope.updateDisplay = function () {
            $scope.pageChanged();
        };
//	$scope.setPage = function (pageNo) {
//	    $scope.currentPage = pageNo;
//	};
        $scope.init = function () {

            var user = SessionService.getUser();
            if (user === undefined || user === null) {
                console.log('chua dang nhap');
                window.location.replace('#/login');
                return;
            }
            console.log('dang nhap roi');

            $scope.$route = $route;
            //Pagination
            $scope.totalItems = 0;
            $scope.itemPerPage = 10;
            $scope.currentPage = 1;
            var sessionId = SessionService.getSessionId();
            if (sessionId)
            {
                $scope.updateDisplay();
            } else {
                $scope.updateDisplay();
            }
        };
        $scope.init();
    }
]);