/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('LoginCtrl', ['$scope', '$resource', '$cookieStore', '$window',
    'SessionService', 'UserService',
    function ($scope, $resource, $cookieStore, $window,
            SessionService, UserService) {

        $scope.localUser = {username: '', password: ''};
        $scope.login = function () {

            var resp = UserService.logIn({
                username: $scope.localUser.username,
                password: $scope.localUser.password

            }, function () {
                if (resp.error === 0) {
                    $scope.ok(resp.data.session, resp.data.username, resp.data.userid);
                } else {
                    $scope.showMsg('Login failed');
                }
            });
        };


        $scope.cancel = function () {
            //$modalInstance.dismiss();
            //$window.location.reload();
            $window.location.replace('#/home');
            $window.location.reload();
        };

        $scope.ok = function (session, username, userId) {
            SessionService.logIn(session, username, userId);
            //$window.location.reload();
            $window.location.replace('#/home');
            $window.location.reload();

        };
        $scope.msgList = [];
        $scope.clearMsg = function () {
            $scope.msgList = [];
        };
        $scope.showMsg = function (msg) {
            $scope.msgList = [{type: 'danger', content: msg}];
        };

        $scope.init = function () {
            var user = SessionService.getUser();
            if (user !== undefined) {
                console.log('dang nhap roi');
                window.location.replace('#/home');
                return;
            }
            console.log('chua dang nhap');
        };

        $scope.init();
    }
]);