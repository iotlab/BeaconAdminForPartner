'use strict';

/**
 * @ngdoc service
 * @name myAppApp.ServiceConfig
 * @description
 * # ServiceConfig
 * Factory in the myAppApp.
 */
angular.module('myApp')
	.factory('ServiceConfig', function () {

	    return {
		getUrl: function ()
		{
		    //var url = 'http://lab.updev.org/upm2m/vngdoors/api';
//                    var url = 'http://dev.mapi2.me.zing.vn/frs/mapi2/beacon/partner';
//		    var url = 'http://localhost/vngdoorview/app/db/';
                    var url = 'http://test.me.zing.vn:8501/frs/mapi2/beacon/partner'
		    return url;
		}
	    };
	});
