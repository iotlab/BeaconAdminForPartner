/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
angular.module('myApp')
	.factory('BeaconService', ['$resource', 'ServiceConfig', 'SessionService',
	    function ($resource, ServiceConfig, SessionService) {

		var apiUrl = ServiceConfig.getUrl();
		var sessionId = SessionService.getSessionId();
		return $resource(null,
			{
			    sessionId: sessionId
			},
		{
		    listBeacon: {
			method: 'GET',
			url: apiUrl + '?&method=partner.getbeacons'
		    },  
                    listBeaconAdvertis: {
			method: 'GET',
			url: apiUrl + '?&method=partner.beaconAdvertis'
		    },
                    assignOrRemoveBeaconAdvertis: {
                        method: 'GET',
			url: apiUrl + '?&method=partner.assigOrRemoveAdvertis'
                    },
                    addBeacon: {
			method: 'GET',
			url: apiUrl + '?method=partner.put'
		    },
		    removeBeacon: {
			method: 'GET',
			url: apiUrl + '?method=partner.remove_beacon'
		    }
		});
	    }]);
