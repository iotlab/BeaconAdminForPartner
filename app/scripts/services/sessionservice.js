'use strict';

/**
 * @ngdoc service
 * @name vngdoorviewApp.SessionService
 * @description
 * # SessionService
 * Factory in the vngdoorviewApp.
 */
angular.module('myApp')
	.factory('SessionService', ['$cookieStore', '$window', function ($cookieStore, $window) {

		return {
		    logIn: function (session, username, userId) {
			var user = {session: session, username: username, userid: userId};
			$cookieStore.put('user', user);
		    },
		    logOut: function () {
			$cookieStore.remove('user');
			$window.location.reload();
		    },
		    getUser: function () {
			return $cookieStore.get('user');
		    },
		    getSessionId: function ()
		    {
			var user = this.getUser();
			if (user !== undefined) {
                            console.log('user.session: ' + user.session + ' user.username: ' + user.username);
			    return user.session;
			}
			return null;
		    }
		};
	    }]);
